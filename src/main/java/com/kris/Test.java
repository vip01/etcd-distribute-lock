package com.kris;

/**
 * @author kris
 * @date 2020/8/31 0031 15:37
 */
public class Test {
    public static void main(String[] args) {
        // 模拟分布式场景下，多个进程 “抢锁”
        for (int i = 0; i < 5; i++) {
            new MyThread().start();
        }
    }

    public static class MyThread extends Thread {
        @Override
        public void run() {
            String lockName = "/lock/mylock3";
            // 1\. 加锁
            DistributedLock.LockResult lockResult = DistributedLock.getInstance().lock(lockName, 5,3);

            //可重入测试
            DistributedLock.LockResult lockResult2 = DistributedLock.getInstance().lock(lockName, 5,3);

            System.out.println(lockResult.getIsLockSuccess());
            System.out.println(lockResult2.getIsLockSuccess());
            // 2\. 执行业务
            if (lockResult.getIsLockSuccess()) {
                long sum = 0;
                for (long i = 0; i < 8311111111L; i++) {
                    sum+=i;
                }
                System.out.println(sum);
            }

            // 3\. 解锁
            DistributedLock.getInstance().unLock(lockName, lockResult);
            DistributedLock.getInstance().unLock(lockName, lockResult2);
        }
    }
}
